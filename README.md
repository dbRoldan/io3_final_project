# Universidad Distrital Francisco José de Caldas
## Facultad Ingenieria
### Investigacion de Operaciones III
![UDistrital](public/assets/images/udistrital_esc.png "Universidad Distrital Francisco José de Caldas")


#### Creador:
Daissi Bibiana Gonzalez Roldan   **Codigo:** 20152020108

Natalia Angarita Buitrago   **Codigo:** 20152020645

Isabel Victoria Pérez Díaz   **Codigo:** 20161020014

Andrés Leonardo Arias Uribe   **Codigo:** 20161020029

#### Pagina Web:
Proyecto de pagina Web Projecto Final de materia
#### Descripcion:
Se estructuro pagina web **Inteligencia Visual** como un proyecto orientado a describir aun proceso o metodo de apredizaje a traves de la realizacion de pruebas para un estudiante que posea la ya mencionada **Inteligencia Visual** como inteligencia dominante, permitiendole consecuentemente al docente una herramienta para el desarrollo de una metodologia de aprendizaje mas adecuada segun las teorias previamente analizadas.
#### Herramientas:
* HTML5
* JavaScript
* CSS3

![Tec](public/assets/images/tec.png " Tec")
* Bootstrap
![Boostrap](public/assets/images/bootstrap.png " Boostrap")
* Phaser FrameWork
![Phaser](public/assets/images/phaser.png " Phaser")
#### Link:
[Pagina Web](https://dbroldan.gitlab.io/io3_final_project/ " Pagina Web - Inteligencia Visual")

#### Licencia:
MIT License
